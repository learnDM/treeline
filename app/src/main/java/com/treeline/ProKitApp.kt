package com.treeline

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
open class ProKitApp : Application()