package com.treeline.domain.model


data class SoldProduct(

    var id: Long = 0,
    var idProduct: Long = 0,
    val title: String = "",
    var amount: Int = 0,
    var dataOfSold: String = "",
    var dataLong: Long = 0L
)
