package com.treeline.domain.repository

import com.treeline.data.local.entities.UserEntity

interface AuthRepository {


    suspend fun saveUser(user: UserEntity)
    suspend fun getUser(): UserEntity?
}