package com.treeline.domain.repository

import com.treeline.data.remote.dto.ProductDto
import com.treeline.domain.model.SoldProduct
import retrofit2.Response


interface MainRepository {

    suspend fun getInventory(): Response<List<ProductDto>>
    suspend fun getProductsByTitle(title: String): Response<List<ProductDto>>
    suspend fun getOneProduct(userID: Long): Response<ProductDto>
    suspend fun getTodaySoldProducts(startday: Long, endday: Long): Response<List<SoldProduct>>
    suspend fun decrease(prodId: Long): Response<SoldProduct>

}