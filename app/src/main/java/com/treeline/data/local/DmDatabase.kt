package com.treeline.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.treeline.data.local.dao.UserDao
import com.treeline.data.local.entities.UserEntity


@Database(
    entities = arrayOf(UserEntity::class),
    version = 1
)
abstract class DmDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

}