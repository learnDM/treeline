package com.treeline.data.repository

import com.treeline.data.local.DmDatabase
import com.treeline.data.local.entities.UserEntity
import com.treeline.domain.repository.AuthRepository


class AuthRepositoryImpl(
    private val unsplashDatabase: DmDatabase
) : AuthRepository {

    private val userDao = unsplashDatabase.userDao()

    override suspend fun saveUser(user: UserEntity) = userDao.saveUser(user = user)
    override suspend fun getUser(): UserEntity? = userDao.getUser()
//    suspend fun getUsers(): List<UserEntity> = userDao.getUsers()

}
