package com.treeline.data.repository

import com.treeline.data.remote.RetrofitApiService
import com.treeline.data.remote.dto.ProductDto
import com.treeline.domain.model.SoldProduct
import com.treeline.domain.repository.MainRepository
import retrofit2.Response

class MainRepositoryImpl(
    private val service: RetrofitApiService
) : MainRepository {

    override suspend fun getInventory(): Response<List<ProductDto>> = service.getInventory()

    override suspend fun getProductsByTitle(title: String): Response<List<ProductDto>> =
        service.getProductsByTitle(title)

    override suspend fun getOneProduct(userID: Long): Response<ProductDto> =
        service.getOneProduct(userID)

    override suspend fun getTodaySoldProducts(
        startday: Long,
        endday: Long
    ): Response<List<SoldProduct>> =
        service.getTodaySoldProducts(startday, endday)

    override suspend fun decrease(prodId: Long): Response<SoldProduct> = service.decrice(prodId)


}