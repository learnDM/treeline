package com.treeline.data.remote.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProductDto(

    @Json(name = "id") val productId: String = "",
    val type: String = "",
    val color: String = "",
    val available: Int = 0,
    @Json(name = "title")  val title: String = "",
    val description: String = "",
    val price: String = "",

    )