package com.treeline.data.remote


import com.treeline.data.remote.dto.ProductDto
import com.treeline.domain.model.SoldProduct
import retrofit2.Response
import retrofit2.http.*


interface RetrofitApiService {

    @GET("getInventory")
    suspend fun getInventory(): Response<List<ProductDto>>

    @GET("productsbytitle/{title}")
    suspend fun getProductsByTitle(
        @Path("title") title: String
    ): Response<List<ProductDto>>

    @GET("product/{id}")
    suspend fun getOneProduct(
        @Path("id") userID: Long
    ): Response<ProductDto>


    @GET("soldproducts")
    suspend fun getTodaySoldProducts(
        @Query("start") startday: Long,
        @Query("end") endday: Long,
    ): Response<List<SoldProduct>>


    @PUT("product/{id}/1")
    suspend fun decrice(
        @Path("id") userID: Long
    ): Response<SoldProduct>


    companion object{
        //private const val BASE_URL = "http://q11.jvmhost.net/treeline/"
        const val BASE_URL = "https://us-west2-mobile-hiring.cloudfunctions.net/"
    }
}
