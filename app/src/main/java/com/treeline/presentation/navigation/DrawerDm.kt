package com.treeline.presentation.navigation

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp


@Composable
fun DrawerDm(
    modifier: Modifier = Modifier,
    goToSetMyName: () -> Unit,
) {
    Column(
        modifier
            .fillMaxSize()
            .padding(start = 24.dp, top = 48.dp)
    ) {

        Spacer(Modifier.height(24.dp))

        Text(
            text = "Enter my name",
            modifier.clickable { goToSetMyName() },
            style = MaterialTheme.typography.h4
        )

    }
}

@Preview
@Composable
fun CraneDrawerPreview() {
    DrawerDm(Modifier) {}
}
