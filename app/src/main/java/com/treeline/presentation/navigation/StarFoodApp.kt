package com.treeline.presentation.navigation

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import androidx.paging.ExperimentalPagingApi
import com.google.accompanist.pager.ExperimentalPagerApi
import com.treeline.data.local.entities.UserEntity
import com.treeline.domain.model.SoldProduct
import com.treeline.presentation.screens.LoginScreen
import com.treeline.presentation.screens.MainScreen
import com.treeline.presentation.screens.ProductScreen
import com.treeline.presentation.screens.TodaySoldScreen


@ExperimentalPagingApi
@ExperimentalPagerApi
@ExperimentalFoundationApi
@Composable
fun StarFoodApp(
    navController: NavHostController,
    sendEmail: (List<SoldProduct>, UserEntity?) -> Unit
) {
    NavHost(
        navController,
        startDestination = "main_screen"
    ) {

        composable(route = "main_screen") {
            MainScreen(
                clickProduct = { navController.navigate("product/${it.productId}") },
                goToListSoldProducts = { navController.navigate("today_screen") },
                goToSetMyName = { navController.navigate("setmyname_screen") },
                )
        }

        composable(route = "today_screen") {
            TodaySoldScreen(
                sendEmail = sendEmail
            )
        }

        composable(route = "setmyname_screen") {
            LoginScreen( goToMainScreen = {
                navController.navigate("main_screen")
            })
        }

        composable(
            route = "product/{productId}",
            arguments = listOf(
                navArgument("productId") {
                    // Make argument type safe
                    type = NavType.LongType
                }
            ),

            content = { entry ->
                val productId = entry.arguments?.getLong("productId")
                productId?.let {
                    ProductScreen(productId = it)
                }

            })

    }
}

