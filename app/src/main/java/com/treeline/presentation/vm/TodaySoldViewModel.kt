package com.treeline.presentation.vm

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.treeline.domain.repository.MainRepository
import com.treeline.domain.model.SoldProduct
import com.treeline.utils.Dates
import com.treeline.utils.toLongEndDay
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class TodaySoldViewModel @Inject constructor(
    private val mainRepository: MainRepository
) : ViewModel() {


    var listTodaySold by mutableStateOf<List<SoldProduct>>(listOf())

    var isLoading = mutableStateOf(false)
    val isError = mutableStateOf(false)

    init {
        loadTodaySoldProducts()
    }

    private fun loadTodaySoldProducts() {
        viewModelScope.launch() {
            isLoading.value = true
            val startday = Dates.longToLongStart(System.currentTimeMillis())
            val endday = System.currentTimeMillis().toLongEndDay()
            val resp: Response<List<SoldProduct>> =
                mainRepository.getTodaySoldProducts(startday, endday)

            if (resp.isSuccessful) {
                val listResult = resp.body()
                listTodaySold = listResult ?: listOf()

            } else {
                isLoading.value = false
                isError.value = true
            }
            isLoading.value = false
        }

    }
}