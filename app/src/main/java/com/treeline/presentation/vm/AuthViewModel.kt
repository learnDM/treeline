package com.treeline.presentation.vm

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.treeline.data.local.entities.UserEntity
import com.treeline.domain.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(
    private val repository: AuthRepository
) : ViewModel() {

    var user: UserEntity? by mutableStateOf(null)

    init {
        getUser()
    }

    fun saveUser(user: UserEntity, goToMainScreen: () -> Unit) {
        viewModelScope.launch() {
            repository.saveUser(user = user)
            goToMainScreen.invoke()
        }
    }

    fun getUser() {
        viewModelScope.launch() {
            user = repository.getUser()
        }
    }

}