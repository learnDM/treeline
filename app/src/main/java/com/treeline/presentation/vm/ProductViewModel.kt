package com.treeline.presentation.vm

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.treeline.domain.repository.MainRepository
import com.treeline.data.remote.dto.ProductDto
import com.treeline.domain.model.SoldProduct
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject


@HiltViewModel
class ProductViewModel @Inject constructor(private val mainRepository: MainRepository) :
    ViewModel() {

    var product: MutableState<ProductDto> = mutableStateOf(ProductDto())
    var isLoading = mutableStateOf(false)
    val isError = mutableStateOf(false)


    fun decrice(prodId: Long) {
        viewModelScope.launch {
            isLoading.value = true

            val resp: Response<SoldProduct> = mainRepository.decrease(prodId = prodId)

            if (resp.isSuccessful) {

                val soldProduct = resp.body()
                soldProduct?.let {
                    getProductById(prodId)
                }

            } else {
                isLoading.value = false
                isError.value = true
            }

            isLoading.value = false
        }
    }


    fun getProductById(userID: Long) {
        viewModelScope.launch {
            isLoading.value = true

            val resp: Response<ProductDto> = mainRepository.getOneProduct(userID)

            if (resp.isSuccessful) {

                resp.body()?.let {
                    product.value = it
                    isLoading.value = false
                }
            } else {
                isLoading.value = false
                isError.value = true
            }
        }
    }
}