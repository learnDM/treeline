package com.treeline.presentation.vm

import androidx.compose.runtime.*
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.treeline.domain.repository.MainRepository
import com.treeline.data.remote.dto.ProductDto
import com.treeline.presentation.screens.SearchWidgetState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject


@HiltViewModel
class MainViewModel @Inject constructor(
    private val mainRepository: MainRepository
) : ViewModel() {

    var productLive by mutableStateOf<List<ProductDto>>(listOf())

    var isLoading by mutableStateOf(false)
    val isError = mutableStateOf(false)

    private val _searchWidgetState: MutableState<SearchWidgetState> =
        mutableStateOf(value = SearchWidgetState.CLOSED)
    val searchWidgetState: State<SearchWidgetState> = _searchWidgetState

    private val _searchTextState: MutableState<String> = mutableStateOf(value = "")
    val searchTextState: State<String> = _searchTextState

    fun updateSearchWidgetState(newValue: SearchWidgetState) {
        _searchWidgetState.value = newValue
    }

    fun updateSearchTextState(newValue: String) {
        _searchTextState.value = newValue
    }

    fun loadProducts(title: String? = null) {
        viewModelScope.launch {
            isLoading = true
            isError.value = false

            val resp: Response<List<ProductDto>> =
                if (title == null) mainRepository.getInventory()
                else mainRepository.getProductsByTitle(title)

            if (resp.isSuccessful) {
                val listResult = resp.body()
                productLive = listResult ?: listOf()

            } else {
                isLoading = false
                isError.value = true
            }
            isLoading = false
        }
    }

}