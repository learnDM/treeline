package com.treeline.presentation.screens


import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Fastfood
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.treeline.data.remote.dto.ProductDto
import com.treeline.presentation.components.ErrorUI
import com.treeline.presentation.components.FloatingButtonAdd
import com.treeline.presentation.components.LoadingUI
import com.treeline.presentation.components.isScrollingUp
import com.treeline.presentation.navigation.DrawerDm
import com.treeline.utils.height
import com.treeline.presentation.vm.MainViewModel

@Composable
fun MainScreen(
    clickProduct: (ProductDto) -> Unit = {},
    goToListSoldProducts: () -> Unit,
    goToSetMyName: () -> Unit,
) {

    var searchtext by rememberSaveable { mutableStateOf("") }
    val vm: MainViewModel = hiltViewModel()

    if (searchtext.isEmpty()) vm.loadProducts() else vm.loadProducts(searchtext)

    val isError: Boolean by vm.isError

    val lazyListState = rememberLazyListState()

    val searchWidgetState by vm.searchWidgetState
    val searchTextState by vm.searchTextState

    Scaffold(
        drawerContent = {
            DrawerDm(Modifier, goToSetMyName = goToSetMyName)
        },
        topBar = {
            MainAppBar(
                searchWidgetState = searchWidgetState,
                searchTextState = searchTextState,
                onTextChange = {
                    searchtext = it
                    vm.updateSearchTextState(newValue = it)
                },
                onCloseClicked = {
                    vm.updateSearchWidgetState(newValue = SearchWidgetState.CLOSED)
                    vm.loadProducts()
                },
                onSearchClicked = {
                    Log.d("Searched Text", it)
                    searchtext = it
                    vm.loadProducts(it)
                },
                onSearchTriggered = {
                    vm.updateSearchWidgetState(newValue = SearchWidgetState.OPENED)
                }
            )
        },

        floatingActionButton = {
            FloatingButtonAdd(
                "Sold today",
                lazyListState.isScrollingUp(),
                Icons.Default.Fastfood
            ) {
                goToListSoldProducts()
            }
        },

        content = {
            when {
                vm.isLoading -> LoadingUI()
                isError -> {
                    Log.d("gg", "dm:: Some Error")
                    ErrorUI()
                }
                else -> {
                    Log.d("gg", "dm:: 2222")
                    LazyColumn(Modifier.padding(4.dp), state = lazyListState) {
                        items(items = vm.productLive) {
                            DishItem(it) { clickProduct(it) }
                        }

                        item {
                            60.height()
                        }
                    }
                }
            }
        }
    )

}


@Composable
fun DishItem(item: ProductDto, clickDish: (ProductDto) -> Unit = {}) =
    Card(
        shape = MaterialTheme.shapes.small,
        modifier = Modifier.padding(4.dp),
        elevation = 8.dp
    ) {
        Row(
            modifier = Modifier
                .padding(bottom = 16.dp, start = 16.dp, end = 16.dp)
                .fillMaxWidth()
                .clickable { clickDish(item) },
            content = {

                Column() {
                    2.height()
                    Text("ID: ${item.productId}")

                    2.height()
                    Text("Title: ${item.title}")

                    2.height()
                    Text("Type: ${item.type}")

                    2.height()
                    Text("Available: ${item.available}")

                    2.height()
                    val text =
                        if (item.description.length > 50) item.description.substring(0, 50)
                            .plus("..")
                        else item.description
                    Text(text)

                }
            })

    }


enum class SearchWidgetState {
    OPENED, CLOSED
}

@Composable
fun MainAppBar(
    searchWidgetState: SearchWidgetState,
    searchTextState: String,
    onTextChange: (String) -> Unit,
    onCloseClicked: () -> Unit,
    onSearchClicked: (String) -> Unit,
    onSearchTriggered: () -> Unit
) {
    when (searchWidgetState) {
        SearchWidgetState.CLOSED -> DefaultAppBar(onSearchClicked = onSearchTriggered)
        SearchWidgetState.OPENED -> {
            SearchAppBar(
                text = searchTextState,
                onTextChange = onTextChange,
                onCloseClicked = onCloseClicked,
                onSearchClicked = onSearchClicked
            )
        }
    }
}

@Composable
fun DefaultAppBar(onSearchClicked: () -> Unit) =
    TopAppBar(
        title = { Text("Search page") },
        actions = {
            IconButton(
                onClick = { onSearchClicked() },
                content = {
                    Icon(
                        imageVector = Icons.Filled.Search,
                        contentDescription = "Search Icon",
                        tint = Color.White
                    )
                })
        }
    )


@Composable
fun SearchAppBar(
    text: String,
    onTextChange: (String) -> Unit,
    onCloseClicked: () -> Unit,
    onSearchClicked: (String) -> Unit,
) {
    Surface(
        modifier = Modifier
            .fillMaxWidth()
            .height(56.dp),
        elevation = AppBarDefaults.TopAppBarElevation,
        color = MaterialTheme.colors.primary,
        content = {
            TextField(
                modifier = Modifier
                    .fillMaxWidth(),
                value = text,
                onValueChange = { onTextChange(it) },
                placeholder = {
                    Text(
                        modifier = Modifier.alpha(ContentAlpha.medium),
                        text = "Search here...",
                        color = Color.White
                    )
                },
                textStyle = TextStyle(fontSize = MaterialTheme.typography.subtitle1.fontSize),
                singleLine = true,
                leadingIcon = {
                    IconButton(
                        modifier = Modifier
                            .alpha(ContentAlpha.medium),
                        onClick = {},
                        content = {
                            Icon(Icons.Default.Search, "Search Icon", tint = Color.Red)
                        })
                },
                trailingIcon = {
                    IconButton(
                        onClick = {
                            if (text.isNotEmpty()) onTextChange("") else onCloseClicked()
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Default.Close,
                            contentDescription = "Close Icon",
                            tint = Color.White
                        )
                    }
                },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search),
                keyboardActions = KeyboardActions(onSearch = { onSearchClicked(text) }),
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent,
                    cursorColor = Color.White.copy(alpha = ContentAlpha.medium)
                )
            )
        }
    )
}
