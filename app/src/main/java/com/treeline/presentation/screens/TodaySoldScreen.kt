package com.treeline.presentation.screens

import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.treeline.data.local.entities.UserEntity
import com.treeline.domain.model.SoldProduct
import com.treeline.presentation.components.ErrorUI
import com.treeline.presentation.components.LoadingUI
import com.treeline.utils.height
import com.treeline.presentation.vm.AuthViewModel
import com.treeline.presentation.vm.TodaySoldViewModel


@Composable
fun TodaySoldScreen(
    sendEmail: (List<SoldProduct>, UserEntity?) -> Unit
) {

    val vm: TodaySoldViewModel = hiltViewModel()
    val authVM: AuthViewModel = hiltViewModel()

    val list = vm.listTodaySold

    val isError: Boolean by vm.isError
    val isLoading: Boolean by vm.isLoading

    val lazyListState = rememberLazyListState()

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text("Sold today") },
                actions = {
                    IconButton(
                        onClick = { sendEmail.invoke(list, authVM.user) },
                        content = {
                            Text(text = "Send to boss")
                        }
                    )
                }
            )
        },
        content = {
            when {
                isLoading -> LoadingUI()
                isError -> {
                    Log.d("gg", "dm:: Some Error")
                    ErrorUI()
                }
                else -> {
                    Log.d("gg", "dm:: 2222")
                    LazyColumn(Modifier.padding(4.dp), state = lazyListState) {
                        items(items = list) {
                            SoldItem(it) { }
                        }

                        item {
                            60.height()
                        }
                    }
                }
            }
        }
    )
}


@Composable
fun SoldItem(item: SoldProduct, clickDish: (SoldProduct) -> Unit = {}) =
    Card(
        shape = MaterialTheme.shapes.small,
        modifier = Modifier.padding(4.dp),
        elevation = 8.dp
    ) {
        Row(
            modifier = Modifier
                .padding(bottom = 16.dp, start = 16.dp, end = 16.dp)
                .fillMaxWidth()
                .clickable { clickDish(item) },
            content = {
                Text("ID: ${item.idProduct} (${item.dataOfSold})")
            })
    }