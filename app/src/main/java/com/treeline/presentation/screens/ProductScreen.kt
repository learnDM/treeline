package com.treeline.presentation.screens

import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.treeline.data.remote.dto.ProductDto
import com.treeline.utils.radius
import com.treeline.presentation.vm.ProductViewModel

data class DialogState(var showDialog: Boolean)

@Composable
fun ProductScreen(productId: Long) {

    val vm: ProductViewModel = hiltViewModel()
    val product: ProductDto by vm.product
    vm.getProductById(productId)

    var dialogState by remember { mutableStateOf(DialogState(false)) }
    if (dialogState.showDialog) {
        ShowDialog() {
            Log.d("ff", "dm:: pressed 'Ok'")
            dialogState = dialogState.copy(showDialog = false)
        }
    }


    Column(modifier = Modifier.padding(16.dp)) {
        Text(text = "ID: ${product.productId}")
        Text(text = "Title: ${product.title}")
        Text(text = "Description: ${product.description}")
        Text(text = "Available: ${product.available}")

        Button(
            onClick = {
                if (product.available > 0) {
                    vm.decrice(prodId = productId)
                } else {
                    dialogState = DialogState(true)
                }
            },
            modifier = Modifier.fillMaxWidth().padding(top = 36.dp),
            shape = 40.radius(),
            content = {
                Text(
                    "Sell",
                    modifier = Modifier.padding(8.dp)
                )
            }
        )
    }

}


@Composable
fun ShowDialog(
    text: String = "Please fill all fields",
    onClickOk: () -> Unit
) {

    AlertDialog(
        onDismissRequest = {},
        buttons = {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 16.dp, end = 16.dp),
                horizontalArrangement = Arrangement.End,
                verticalAlignment = Alignment.Top
            ) {
                Button(onClick = onClickOk) {
                    Text(  text = "Ok")
                }
            }
        },
        title = { Text(text = "Not filled" ) },
        text = { Text(text) },
        backgroundColor =  MaterialTheme.colors.surface
    )

}