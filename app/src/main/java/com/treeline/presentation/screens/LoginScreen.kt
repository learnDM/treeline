package com.treeline.presentation.screens

import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.paging.ExperimentalPagingApi
import com.treeline.data.local.entities.UserEntity
import com.treeline.utils.height
import com.treeline.utils.radius
import com.treeline.presentation.vm.AuthViewModel


@ExperimentalPagingApi
@Composable
fun LoginScreen(goToMainScreen: () -> Unit) {

    val vm: AuthViewModel = hiltViewModel()

    var userName by remember { mutableStateOf(TextFieldValue("")) }
    val userNameErrorState = remember { mutableStateOf(false) }


    Box(modifier = Modifier.fillMaxSize()) {

        LazyColumn(
            modifier = Modifier.padding(16.dp),
            content = {
                item {

                    10.height()
                    Text("Enter your name and save in DB")
                    24.height()
                    OutlinedTextField(
                        value = userName,
                        modifier = Modifier
                            .fillMaxWidth(),
                        label = { Text(text = "Username or mobile number") },

                        shape = 40.radius(),
                        placeholder = {
                            Text(text = "Username or mobile number")
                        },
                        isError = userNameErrorState.value,
                        onValueChange = {
                            if (userNameErrorState.value) {
                                userNameErrorState.value = false
                            }
                            userName = it
                        }
                    )
                    if (userNameErrorState.value) {
                        Text(text = "Required name of user!")
                    }

                    26.height()

                    Button(
                        modifier = Modifier.fillMaxWidth(),
                        shape = 40.radius(),
                        content = {
                            Text(
                                text = "Save name in DB",
                                modifier = Modifier.padding(8.dp)
                            )
                        },
                        onClick = {
                            when {
                                userName.text.isEmpty() -> userNameErrorState.value = true
                                else -> {
                                    userNameErrorState.value = false

                                    Log.d("gg", "dm:: userName: ${userName.text} ")

                                    vm.saveUser(UserEntity(name = userName.text, email = "555@gmail.com"))
                                    { goToMainScreen() }
                                }
                            }
                        },
                    )
                    26.height()

                }
            },
        )
    }
}


@ExperimentalPagingApi
@Preview
@Composable
fun LoginScreen_Preview() {
    LoginScreen {}
}

