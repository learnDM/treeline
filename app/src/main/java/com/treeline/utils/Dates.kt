package com.treeline.utils

import android.util.Log
import java.text.SimpleDateFormat
import java.util.*

 const val COMMON_PATTERN = "dd.MM.yyyy"
private const val TIME_WITH_MILLISECONDS = "dd.MM.yyyy HH:mm:ss"

object Dates {

     fun stringToLong(dateStr: String, pattern: String): Long {
        val sdf = SimpleDateFormat(pattern)
        val mDate = sdf.parse(dateStr)
        val long = mDate.time
        return long
    }

    fun longToLongStart(dateLong: Long) : Long {
        val strDate = dateLong.toStringDate(COMMON_PATTERN)
        val strDateStart = "$strDate 00:00:01"
        Log.d("aa", "dm:: strDatStart = $strDateStart")
       return stringToLong(strDateStart, TIME_WITH_MILLISECONDS)
    }
}

fun Long.toStringDate(pattern: String): String =
    if (this != 0L) {
        val date = Date(this)
        val format = SimpleDateFormat(pattern)
        val dateStr = format.format(date)
        dateStr
    } else { "" }

fun Long.toLongEndDay() : Long {
    val strDate = this.toStringDate(COMMON_PATTERN)
    val strDateEnd = "$strDate 23:59:59"
    Log.d("aa", "dm:: strDateEnd = $strDateEnd")
    return Dates.stringToLong(strDateEnd, TIME_WITH_MILLISECONDS)
}