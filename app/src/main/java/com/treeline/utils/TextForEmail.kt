package com.treeline.utils

import com.treeline.domain.model.SoldProduct


fun List<SoldProduct>.toTextForEmail(): String {

    val text = this.groupingBy {
        it.title
    }.eachCount()
        .toList()
        .map {
            "product ${it.first} - sold ${it.second} items"
        }.joinToString("\n")

    val summ = this.sumOf {
        it.amount
    }
    return "Sold:\n $text \n All sold: $summ"
}