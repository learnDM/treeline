package com.treeline.di

import com.treeline.data.local.DmDatabase
import com.treeline.data.remote.RetrofitApiService
import com.treeline.data.repository.AuthRepositoryImpl
import com.treeline.domain.repository.MainRepository
import com.treeline.data.repository.MainRepositoryImpl
import com.treeline.domain.repository.AuthRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideMainRepository(retrofitService: RetrofitApiService): MainRepository =
        MainRepositoryImpl(service = retrofitService)

    @Singleton
    @Provides
    fun provideAuthRepository(database: DmDatabase): AuthRepository =
        AuthRepositoryImpl(unsplashDatabase = database)

}

