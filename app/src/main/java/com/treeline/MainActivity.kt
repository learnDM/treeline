package com.treeline

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.navigation.compose.rememberNavController
import androidx.paging.ExperimentalPagingApi
import com.google.accompanist.pager.ExperimentalPagerApi
import com.treeline.data.local.entities.UserEntity
import com.treeline.domain.model.SoldProduct
import com.treeline.presentation.navigation.StarFoodApp
import com.treeline.ui.theme.TreelineTheme
import com.treeline.utils.toTextForEmail
import dagger.hilt.android.AndroidEntryPoint

@ExperimentalFoundationApi
@ExperimentalPagerApi
@ExperimentalPagingApi
@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TreelineTheme {
                val navController = rememberNavController()
                StarFoodApp(navController) { list, user ->
                    sendEmail(list, user)
                }
            }
        }
    }

    private fun sendEmail(list: List<SoldProduct>, user: UserEntity?) {

        val text: String = list.toTextForEmail()
        val subject: String =
            if (!user?.name.isNullOrEmpty()) "Report your daily sales from ${user?.name}" else "Report your daily sales"

        startActivity(
            Intent.createChooser(
                Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"))
                    .apply {
                        putExtra(Intent.EXTRA_EMAIL, arrayOf("bossman@bosscompany.com"))
                        putExtra(Intent.EXTRA_SUBJECT, subject)
                        putExtra(Intent.EXTRA_TEXT, text)
                    },
                "Send request to boss."
            )
        )
    }
}

